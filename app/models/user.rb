class User < ApplicationRecord
  PASSWORD_MINIMUM_LENGTH = 8
  NAME_MINIMUM_LENGTH = 5
  validates :password, length: { minimum: PASSWORD_MINIMUM_LENGTH }, allow_blank: false, if: :require_password_validation?
  validates :name, length: { minimum: NAME_MINIMUM_LENGTH }, allow_blank: false, on: :update, if: :require_name_validation?
  validates_uniqueness_of :email
  validates_uniqueness_of :reset_password_token, on: :create
  validates_format_of :email, with: /\A[^@\s]+@([^@\s]+\.)+[^@\s]+\z/
  has_secure_password

  def require_name_validation?
    changed.include?("name")
  end

  def require_password_validation?
    changed.include?("password_digest")
  end
end
