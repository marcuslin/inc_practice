class SessionsController < ApplicationController
  def new
  end

  def create
    user = User.find_by(email: session_params[:email])

    if user && user.authenticate(session_params[:password])
      session[:user_id] = user.id

      flash[:notice] = "Welcome back, #{ user.name.capitalize }"
      redirect_to edit_user_path(user)
    else
      flash[:error] = "Incorrect Email or Password"

      render :new
    end
  end

  def destroy
    session[:user_id] = nil

    flash[:notice] = "Successfully logged out"
    redirect_to new_session_path
  end

  private

  def session_params
    params.require(:session).permit(:email, :password)
  end
end
