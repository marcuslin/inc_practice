class UserNotifications < ApplicationMailer
  def welcome_mail
    @user = params[:user]

    mail subject: 'Welcome to Test Site', to: @user.email
  end

  def reset_password_instruction
    @user = params[:user]

    mail subject: 'Welcome to Test Site', to: @user.email
  end
end
