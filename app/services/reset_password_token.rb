class ResetPasswordToken
  TOKEN_EXPIRE_DURATION = 6.hours
  attr_accessor :user, :success

  def initialize(user)
    @user = user
  end

  def generate_token
    return if user.nil?

    user.reset_password_token = SecureRandom.base64(20)
    user.reset_token_expired_at = DateTime.now + TOKEN_EXPIRE_DURATION

    user.save

    UserNotifications.with(user: user).reset_password_instruction.deliver_now
  end

  def valid?
    user.present? && user.reset_token_expired_at > DateTime.now
  end
end
