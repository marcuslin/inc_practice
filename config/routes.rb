Rails.application.routes.draw do
  root 'users#new'

  resources :users
  resources :sessions, only: [:new, :create, :destroy]

  get 'request_password_reset', to: 'passwords#request_reset'
  post 'generate_password_reset_token', to: 'passwords#generate_reset_token'
  get 'reset_password/:reset_password_token', to: 'passwords#reset_password', as: 'reset_password'
  post 'update', to: 'passwords#update', as: 'update_password'
end
