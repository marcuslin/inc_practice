class PasswordsController < ApplicationController
  before_action :fetch_user, only: :update

  def request_reset;end

  def generate_reset_token
    user = User.find_by(email: user_params[:email])
    reset_token_service = ResetPasswordToken.new(user)

    reset_token_service.generate_token

    flash[:notice] = "The reset password link has been sent via email"
    redirect_back(fallback_location: request_password_reset_path)
  end

  def reset_psssword;end

  def update
    reset_token = ResetPasswordToken.new(@user)

    if !reset_token.valid?
      flash[:error] = "Invalid reset password link, please request reset password again"

      redirect_to(request_password_reset_path) and return
    end

    if @user.update_attributes(user_params)
      flash[:notice] = "Password updated"

      redirect_to new_session_path
    else
      flash[:error] = @user.errors.full_messages.first

      redirect_back(fallback_location: update_password_path)
    end
  end

  private

  def fetch_user
    @user = User.find_by(reset_password_token: user_params[:reset_password_token])
  end

  def user_params
    params.require(:user).permit(:email, :reset_password_token, :password, :password_confirmation)
  end
end
