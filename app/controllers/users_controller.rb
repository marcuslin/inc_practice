class UsersController < ApplicationController
  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    @user.name = user_params[:email].split("@").first

    if @user.save
      session[:user_id] = @user.id

      UserNotifications.with(user: @user).welcome_mail.deliver_now

      flash[:notice] = "Welcome, #{ @user.name }"
      redirect_to edit_user_path(@user)
    else
      flash[:error] = @user.errors.full_messages.first

      redirect_to root_url
    end
  end

  def edit
    @user = User.find_by(id: params[:id])
  end

  def update
    @user = User.find_by(id: params[:id])

    if @user.update_attributes(user_params)
    else
      flash[:error] = @user.errors.full_messages.first
    end

    redirect_back(fallback_location: :edit)
  end

  private

  def user_params
    params.require(:user).permit(:name, :email, :password, :password_confirmation)
  end
end
